# image2emoji

This is a small project which reads in an image, and spits out an image made of emoji which looks similar.

It works by splitting the image into 16x16 chunks and comparing it to each emoji, the emoji with the greatest likeness is substituted for that chunk.

This is done for every chunk over multiple threads (it takes around 11 seconds for 4 threads on my laptop for a 700x800 image).

There are some problems with it, like it uses a specific font on my computer, it crops the image to a multiple of 16 among other things. But it is interesting.

This uses [lodepng](https://github.com/lvandeve/lodepng) and [FreeType2](https://www.freetype.org/) as dependencies.

## Building

To build this, do `cmake -S source/ -B build/` in the project root and then do `make -j` in the build directory.

## Execution

The program expects a input filename, output filename and optional font filename argument. If no font is provided a default font is assumed, which may not work on non-Ubuntu machines."

## Notes

I've only verified this works on my laptop (running Ubuntu 20.4.1) and with the font family 'Noto Color Emoji' (by default).

"It works on my machine!"


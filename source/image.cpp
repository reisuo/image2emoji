#include "image.hpp"

#include <cstring>
#include <cmath>
#include <iostream>

#include "lodepng/lodepng.h"

Image::Image() : Image(1, 1) { }

Image::Image(uint32_t width, uint32_t height) :
  data(width * height),
  width(width),
  height(height) { }

void Image::setSubImage(uint32_t x0, uint32_t y0, const Image &image) {
  // TODO: bound check
  for (size_t r = 0; r < image.height; r++) {
    Pixel *destination = location(x0, y0 + r);
    const Pixel *source = image.location(0, r);
    std::memcpy(destination, source, image.width * sizeof(Pixel));
  }
}

bool Image::saveToFile(const char *filename) const {
  uint32_t error = lodepng::encode(
    filename, (const uint8_t *)data.data(), width, height);
  return (error == 0);
}

std::optional<Image> Image::fromFile(const char *filename) {
  std::vector<uint8_t> data;

  Image image;
  uint32_t error = lodepng::decode(
    data, image.width, image.height, filename, LCT_RGBA);
  if (error) {
    std::cerr << "Failed to load image '" << filename << "'.\n";
    return std::nullopt;
  };

  image.data = std::vector<Pixel>(image.width * image.height);

  std::memcpy(
    image.data.data(), data.data(), image.data.size() * sizeof(Pixel)
  );

  return image;
}

LabImage Image::toLab() const {
  LabImage image { std::vector<Lab>(data.size()), width, height };
  for (size_t i = 0; i < data.size(); i++) {
    image.data[i] = Lab(XYZ(data[i]));
  }
  return image;
}

Image Image::cropped(uint32_t newWidth, uint32_t newHeight) const {
  assert((newWidth <= width) && (newHeight <= height));
  
  Image newImage(newWidth, newHeight);

  for (uint32_t y = 0; y < newHeight; y++) {
    Pixel *destination = newImage.location(0, y);
    const Pixel *source = this->location(0, y);
    std::memcpy(destination, source, sizeof(Pixel) * newWidth);
  }

  return newImage;
}

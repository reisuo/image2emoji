#include "emoji_image_generator.hpp"

#include <cstring>
#include <thread>
#include <iostream>

uint32_t min(uint32_t a, uint32_t b) { return (a < b) ? a : b; }

void EmojiImageGenerator::processChunk(uint32_t x0, uint32_t y0) {
  uint32_t bestEmoji = 0;
  float lowestCost = std::numeric_limits<float>::infinity();

  for (uint32_t i = 0; i < emojiCount_; i++) {
    float cost = getChunkEmojiCost(x0, y0, i);
    if (cost >= lowestCost) continue;
    lowestCost = cost;
    bestEmoji = i;
  }

  writeEmoji(x0, y0, bestEmoji);
}

float EmojiImageGenerator::getChunkEmojiCost(
  uint32_t x0, uint32_t y0, uint32_t i
) const {
  float cost = 0.0f;

  for (uint32_t y = 0; y < chunkSize_; y++) {
    for (uint32_t x = 0; x < chunkSize_; x++) {
      const Lab &image = image_.get(
        (x0 * chunkSize_) + x, (y0 * chunkSize_) + y);
      const Lab &emoji = emojiAtlas_.get(x, (i * chunkSize_) + y);
      cost += Lab::sqrDistance(image, emoji);
    }
  }

  return cost;
}

void EmojiImageGenerator::writeEmoji(uint32_t x0, uint32_t y0, uint32_t i) {
  for (uint32_t y = 0; y < chunkSize_; y++) {
    Pixel *destination = result_.location(
      x0 * chunkSize_, (y0 * chunkSize_) + y);
    const Pixel *source = emojiAtlasRGB_.location(0, (i * chunkSize_) + y);
    std::memcpy(destination, source, sizeof(Pixel) * chunkSize_);
  }
}


EmojiImageGenerator::EmojiImageGenerator(
  const LabImage &image, const LabImage &labEmojiAtlas,
  const Image &emojiAtlas, uint32_t chunkSize
) :
  emojiCount_(emojiAtlas.height / chunkSize),
  chunkSize_(chunkSize),
  image_(image),
  emojiAtlas_(labEmojiAtlas),
  emojiAtlasRGB_(emojiAtlas),
  result_(image.width, image.height) {

  const uint32_t chunksAcross = image.width / chunkSize;
  const uint32_t chunksDown = image.height / chunkSize;

  auto job = [&](uint32_t rowStart, uint32_t rowEnd) {
    rowEnd = min(rowEnd, chunksDown);

    for (uint32_t y = rowStart; y < rowEnd; y++)
      for (uint32_t x = 0; x < chunksAcross; x++)
        processChunk(x, y);
  };

  uint32_t threadCount = std::thread::hardware_concurrency();
  if (threadCount == 0) threadCount = 1;
  std::vector<std::thread> threads;

  std::cout << "Finding optimal emoji over " <<
    threadCount << " threads." << std::endl;

  size_t row = 0;
  for (uint32_t _ = 0; _ < (threadCount - 1); _++) {
    size_t end = row + (chunksDown / (threadCount - 1));
    threads.emplace_back(job, row, end);
    row = end;
  }
  job(row, chunksDown);

  for (std::thread &thread : threads) thread.join();
} 

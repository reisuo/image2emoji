#pragma once

#include "font_library.hpp"
#include "image.hpp"

#include <cstdint>
#include <vector>
#include <optional>

typedef struct FT_FaceRec_ FT_FaceRec_;

class FontFace {
public:
  FontFace(
    const FontLibrary &library, const char *filename, uint32_t pixelSize);
  ~FontFace();

  operator bool() const { return face_ != nullptr; }

  std::optional<Image> glyph(uint32_t code) const;
private:
  void shrinkToSize(Image &image) const;

  uint32_t size_;
  FT_FaceRec_ *face_;
};

#pragma once

typedef struct FT_LibraryRec_ FT_LibraryRec_;

class FontLibrary {
public:
  FontLibrary();
  ~FontLibrary();

  operator bool() const { return library_ != nullptr; }
  FT_LibraryRec_ *get() const { return library_; }

private:
  FT_LibraryRec_ *library_;
};

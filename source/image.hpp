#pragma once

#include "pixel.hpp"
#include "lab_image.hpp"

#include <vector>
#include <unordered_map>
#include <cassert>
#include <optional>

struct Image {
  Image();
  Image(uint32_t width, uint32_t height);

  std::vector<Pixel> data;
  uint32_t width;
  uint32_t height;

  Image cropped(uint32_t newWidth, uint32_t newHeight) const;
  LabImage toLab() const;

  Pixel *location(uint32_t x, uint32_t y) {
    assert((x <= width) && (y <= height));
    return &data[(y * width) + x];
  }

  const Pixel *location(uint32_t x, uint32_t y) const {
    assert((x <= width) && (y <= height));
    return &data[(y * width) + x];
  }

  void setSubImage(uint32_t x0, uint32_t y0, const Image &image);

  bool saveToFile(const char *filename) const;

  static std::optional<Image> fromFile(const char *filename);
};

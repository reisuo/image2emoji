#include "font_library.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>

FontLibrary::FontLibrary() {
  FT_Error error = FT_Init_FreeType(&library_);
  if (error != FT_Err_Ok) {
    std::cerr << "Failed to initialise font library: "
      << FT_Error_String(error) << "\n";
    library_ = nullptr;
    return;
  }

  std::cout << "Initialised font library.\n";
}

FontLibrary::~FontLibrary() {
  if (library_ == nullptr) return;
  FT_Done_FreeType(library_);
  std::cout << "Cleaned up font library.\n";
}

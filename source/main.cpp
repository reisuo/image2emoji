#include "font_face.hpp"
#include "codes.hpp"
#include "emoji_image_generator.hpp"

#include <iostream>

std::optional<Image> generateEmojiAtlas(
  const char *fontFile, size_t chunkSize
) {
  std::cout << "Loading emoji...\n";

  const FontLibrary library;
  if (!library) return std::nullopt;
  const FontFace face(library, fontFile, chunkSize);
  if (!face) return std::nullopt;

  constexpr size_t emojiCount = unicode12Emoji.size();
  Image atlas(chunkSize, emojiCount * chunkSize);

  for (size_t i = 0; i < emojiCount; i++) {
    uint32_t code = unicode12Emoji[i];
    std::optional<Image> image = face.glyph(code);
    if (!image) {
      std::cerr << "Failed to load image for emoji with code " << code << ".\n";
      continue;
    }

    atlas.setSubImage(0, i * chunkSize, *image);
  }

  return atlas;
}

constexpr uint32_t roundDown(uint32_t n, uint32_t r) { return (n / r) * r; }

int main(int argc, char **argv) {
  constexpr size_t chunkSize = 16;
  constexpr const char *defaultFontLocation =
    "/usr/share/fonts/truetype/noto/NotoColorEmoji.ttf";

  const char *fontLocation = defaultFontLocation;

  if (argc == 4) {
    fontLocation = argv[3];
  } else if (argc != 3) {
    std::cerr << "Expected 2 or 3 arguments.\nProper usage: "
      << argv[0] << " [in filename] [out filename] <font location>\n";
    return EXIT_FAILURE;
  }

  const char *const inFilename = argv[1];
  const char *const outFilename = argv[2];

  auto maybeImage = Image::fromFile(inFilename);
  if (!maybeImage) return EXIT_FAILURE;
  Image &image = *maybeImage;
  image = image.cropped(
    roundDown(image.width, chunkSize),
    roundDown(image.height, chunkSize)
  );

  const LabImage labImage = image.toLab();

  const auto maybeEmojiAtlas = generateEmojiAtlas(fontLocation, chunkSize);
  if (!maybeEmojiAtlas) return EXIT_FAILURE;
  const Image &emojiAtlas = *maybeEmojiAtlas;
  const LabImage labEmojiAtlas = emojiAtlas.toLab();

  std::cout << "Emojifying image.\n";
  const EmojiImageGenerator generator(
    labImage, labEmojiAtlas, emojiAtlas, chunkSize);  
  const Image &result = generator.result();

  std::cout << "Writing to " << outFilename << ".\n";
  result.saveToFile(outFilename);

  return EXIT_SUCCESS;
}

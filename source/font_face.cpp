#include "font_face.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>
#include <cstring>
#include <cassert>

FontFace::FontFace(
  const FontLibrary &library, const char *filename, uint32_t pixelSize
) : size_(pixelSize) {
  if (!library) return;

  FT_Error error = FT_New_Face(library.get(), filename, 0, &face_);
  if (error != FT_Err_Ok) {
    std::cerr << "Failed to load font face from '" <<
      filename << "': " << error << ".\n";
    face_ = nullptr;
    return;
  }

  std::cout << "Loaded font face from '" << filename
    << "' containing " << face_->num_glyphs << " glyphs.\n";

  // I don't know what this does but it works.
  // What the heck is a "bitmap strike"?
  FT_Select_Size(face_, 0);
  // FT_Set_Char_Size(face_, 0, pixelSize * 64, 0, 300);
}

FontFace::~FontFace() {
  if (face_ == nullptr) return;
  FT_Done_Face(face_);
  std::cout << "Cleaned up a font face.\n";
}

std::optional<Image> FontFace::glyph(uint32_t code) const {
  if (face_ == nullptr) return std::nullopt;

  uint32_t index = FT_Get_Char_Index(face_, code);
  if (index == 0) {
    std::cerr << "Failed to get glyph index for code '" << code << "'\n";
    return std::nullopt;
  }

  FT_Error loadError = FT_Load_Glyph(face_, index, FT_LOAD_COLOR);
  if (loadError != FT_Err_Ok) {
    std::cerr << "Failed to load glyph: " << loadError << "\n";
    return std::nullopt;
  }

  FT_Error renderError = FT_Render_Glyph(face_->glyph, FT_RENDER_MODE_NORMAL);
  if (renderError != FT_Err_Ok) {
    std::cerr << "Failed to render glyph " << renderError << ".\n";
    return std::nullopt;
  }

  const FT_Bitmap &bitmap = face_->glyph->bitmap;
  std::vector<Pixel> data(bitmap.rows * bitmap.width);
  for (uint32_t y = 0; y < bitmap.rows; y++) {
    for (uint32_t x = 0; x < bitmap.width; x++) {
      Pixel pixel;
      size_t i = (y * bitmap.width) + x;
      pixel.b = bitmap.buffer[(i * 4) + 0];
      pixel.g = bitmap.buffer[(i * 4) + 1];
      pixel.r = bitmap.buffer[(i * 4) + 2];
      pixel.a = bitmap.buffer[(i * 4) + 3];
      data[i] = pixel;
    }
  }

  Image image(bitmap.width, bitmap.rows);
  image.data = data;
  shrinkToSize(image);
  return image;
}

void FontFace::shrinkToSize(Image &image) const {
  // I'm writing a simpler implementation with assumed parameters.
  // I'll write a more general one later (maybe).
  assert((image.width == 136) && (image.height == 128));
  assert(size_ == 16);

  std::vector<Pixel> newData(size_ * size_);
  for (size_t y = 0; y < size_; y++) {
    for (size_t x = 0; x < size_; x++) {
      size_t y0 = y * 8;
      size_t x0 = (x * 8) + 4;
      Pixel pixel = image.data[(y0 * image.width) + x0];
      newData[(y * size_) + x] = pixel;
    }
  }

  image.data = newData;
  image.width = size_;
  image.height = size_;
}

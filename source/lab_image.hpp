#pragma once

#include "lab.hpp"

#include <vector>
#include <cstdint>
#include <cassert>

struct LabImage {
  std::vector<Lab> data;
  uint32_t width;
  uint32_t height;

  Lab &get(uint32_t x, uint32_t y) {
    assert((x <= width) && (y <= height));
    return data[(y * width) + x];
  }

  const Lab &get(uint32_t x, uint32_t y) const {
    assert((x <= width) && (y <= height));
    return data[(y * width) + x];
  }
};

#pragma once

#include "xyz.hpp"

struct Lab {
  Lab() = default;
  
  explicit Lab(XYZ xyz);

  static float sqrDistance(Lab p, Lab q);

  float L, a, b;

private:
  static float f(float t);
};

#pragma once

#include "lab.hpp"
#include "rgb.hpp"

#include <cstdint>

struct Pixel {
  uint8_t r, g, b, a;

 operator RGB() const {
   float alpha = a / 255.0f;
   return {
      (r / 255.0f) * alpha,
      (g / 255.0f) * alpha,
      (b / 255.0f) * alpha
    };
  }
};

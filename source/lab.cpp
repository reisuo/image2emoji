#include "lab.hpp"

#include <cmath>

Lab::Lab(XYZ xyz) {
  constexpr float x_n = 95.0489f;
  constexpr float y_n = 100.0f;
  constexpr float z_n = 108.8840f;

  float fX_Xn = f(xyz.x / x_n);
  float fY_Yn = f(xyz.y / y_n);
  float fZ_Zn = f(xyz.z / z_n);
  L = (116.0f * fY_Yn) - 16.0f;
  a = 500 * (fX_Xn - fY_Yn);
  b = 200 * (fY_Yn - fZ_Zn);
}

static float square(float v) { return (v * v); }

float Lab::sqrDistance(Lab p, Lab q) {
  constexpr float k_1 = 0.045f;
  constexpr float k_2 = 0.015f;

  float dL_2 = square(p.L - q.L);
  float dA_2 = square(p.a - q.a);
  float dB_2 = square(p.b - q.b);
  float c1 = std::sqrt(square(p.a) + square(p.b));
  float c2 = std::sqrt(square(q.a) + square(q.b));
  float dC_2 = square(c1 - c2);
  float dH_2 = dA_2 + dB_2 + dC_2;
  float s_c_2 = square(1 + (k_1 * c1));
  float s_h_2 = square(1 + (k_2 * c1));
  float dE_2 = dL_2 + (dC_2 / s_c_2) + (dH_2 / s_h_2);
  return dE_2;
}

float Lab::f(float t) {
  constexpr float sigma = (6.0f / 29.0f);
  constexpr float sigma2 = sigma * sigma;
  constexpr float sigma3 = sigma2 * sigma;
  if (t > sigma3) return std::cbrt(t);
  return (t / (3 * sigma2)) + (4.0f / 29.0f);
}

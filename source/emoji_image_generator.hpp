#pragma once

#include "image.hpp"
#include "lab_image.hpp"

class EmojiImageGenerator {
public:
  EmojiImageGenerator(
    const LabImage &image, const LabImage &labEmojiAtlas,
    const Image &emojiAtlas, uint32_t chunkSize
  );

  const Image &result() const { return result_; };

private:
  void processChunk(uint32_t x, uint32_t y);
  float getChunkEmojiCost(uint32_t x0, uint32_t y0, uint32_t i) const;
  void writeEmoji(uint32_t x0, uint32_t y0, uint32_t i);

  const uint32_t emojiCount_;
  const uint32_t chunkSize_;
  const LabImage &image_;
  const LabImage &emojiAtlas_;
  const Image &emojiAtlasRGB_;
  Image result_;
};

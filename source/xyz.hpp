#pragma once

#include "rgb.hpp"

struct XYZ {
  explicit XYZ(RGB rgb);

  float x, y, z;

private:
  static float inverseGamma(float u);
};

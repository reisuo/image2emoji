#include "xyz.hpp"

#include <array>
#include <cmath>

// https://en.wikipedia.org/wiki/SRGB#The_reverse_transformation
XYZ::XYZ(RGB rgb) {
  constexpr std::array<std::array<float, 3>, 3> m {
    std::array<float, 3> { 0.41239080f, 0.35758434f, 0.18048079f, },
    std::array<float, 3> { 0.21263901f, 0.71516868f, 0.07219232f, },
    std::array<float, 3> { 0.01933082f, 0.11919478f, 0.95053215f, }
  };

  float r = inverseGamma(rgb.r);
  float g = inverseGamma(rgb.g);
  float b = inverseGamma(rgb.b);

  x = (m[0][0] * r) + (m[0][1] * g) + (m[0][2] * b);
  y = (m[1][0] * r) + (m[1][1] * g) + (m[1][2] * b);
  z = (m[2][0] * r) + (m[2][1] * g) + (m[2][2] * b);
}

float XYZ::inverseGamma(float u) {
  if (u <= 0.04045f) return (25.0f * u) / 323.0f;

  float value = ((200.0f * u) + 11.0f) / 211.0f;
  return std::pow(value, 12.0f / 5.0f);
}
